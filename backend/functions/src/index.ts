import * as functions from 'firebase-functions';
const cors = require('cors')({ origin: true });

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});


export const addTwoNumbersOnly = functions.https.onRequest((request, response) => 
 cors(request, response, () => {
  response.status(200).json({sum: request.body.num1 + request.body.num2})
}));
