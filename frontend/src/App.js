import React from 'react';
import logo from './logo.svg';
import './App.css';

function callAddfunction(e, num1, num2) {
  e.preventDefault()
  return fetch('https://us-central1-firebase-backend-demo.cloudfunctions.net/addTwoNumbersOnly', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer', // no-referrer, *client
    body: JSON.stringify({
      "num1": parseInt(num1),
      "num2": parseInt(num2)
    }) // body data type must match "Content-Type" header
  }).then(data => data.json())
}



class App extends React.Component {
  
  state = {
    'num1': 0,
    'num2': 0,
    'sum': 0
  }

  constructor() {


    super()
  }

  onChange1(e) {
    this.setState({'num1': e.target.value})
  }

  onChange2(e) {
    this.setState({'num2': e.target.value})
  }


  render() {

    return (
      <div className="App">
      <header className="App-header">
      <form onSubmit={(e) => callAddfunction(e, this.state.num1, this.state.num2).then(result => this.setState({sum: result.sum}))}>
      <input type='number' value={this.state.num1} onChange={(e) => this.onChange1(e)} placeholder='NUMBER 1'></input>
      <input type='number' value={this.state.num2} onChange={(e) => this.onChange2(e)} placeholder='NUMBER 2'></input>
      <button type='submit'>ADD THIS PLEASE FOR ME!</button>
      </form>
      <p>{this.state.sum}</p>
      </header>
      </div>
      );
    }
  }

export default App;
